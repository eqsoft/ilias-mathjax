FROM ubuntu:bionic

LABEL maintainer="schneider@hrz.uni-marburg.de"

# do not edit
ENV DEBIAN_FRONTEND noninteractive

ARG MATHJAX_PORT=8013
ENV MATHJAX_PORT=$MATHJAX_PORT

# os
RUN apt-get update && apt-get install -y --no-install-recommends \
    software-properties-common \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg \
    nano \
    supervisor
    
# node
RUN apt-get update && curl -sL https://deb.nodesource.com/setup_12.x | bash - \
    && apt-get -y install nodejs

# mathjax
RUN mkdir -p /opt/mathjax \
    && cd /opt/mathjax \
    && npm install https://github.com/mathjax/MathJax-node/tarball/master \
    && npm install https://github.com/tiarno/mathjax-server/tarball/master \
    && cd node_modules \
    && ln -s mathjax-node MathJax-node
    
# copy files
COPY services/init/ /usr/local/bin/
COPY services/mathjax/ /opt/mathjax/
COPY services/supervisor/conf.d/ /etc/supervisor/conf.d/
COPY services/supervisor/docker.conf /etc/supervisor/

RUN chmod 755 /usr/local/bin/*

CMD ["init.sh"]