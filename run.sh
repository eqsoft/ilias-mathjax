docker run \
	-d \
	--rm \
	--name ilias-mathjax-cont \
	-p 8013:8013 \
	registry.gitlab.com/eqsoft/ilias-mathjax:latest
